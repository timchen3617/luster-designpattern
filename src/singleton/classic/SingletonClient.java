package singleton.classic;

public class SingletonClient {
	public static void main(String[] args) {
		
		// illegal construct
		// Compile Time Error: The constructor SingleObject() is not visible
		// SingleObject object = new SingleObject();
		
		// Get the only object available
		Singleton singleton = Singleton.getInstance();
		
		// show the description
		singleton.getDescription();
	}
}
