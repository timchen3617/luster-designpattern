package singleton.classic;

// NOTE: This is not thread safe!
// 經典獨體模式
public class Singleton {
	private static Singleton uniqueInstance;
	
	// make the constructor private so that this class cannot be
	// instantiated
	private Singleton() {}
	
	// Get the only object available
	public static Singleton getInstance() {
		if (uniqueInstance == null) {
			uniqueInstance = new Singleton();
		}
		return uniqueInstance;
	}
 
	// other useful methods here
	public void getDescription() {
		System.out.println("I'm a classic Singleton!");
	}
}
