package singleton.dcl;

// 雙重檢查加鎖

public class Singleton {
	//violate定義的域在發生修改後，它會直接寫到主存中，對其他任務可見
	//用volatile修飾的變數，執行緒在每次開始使用變數的時候，都會讀取變數修改後的最新的值。
	private volatile static Singleton uniqueInstance;
 
	private Singleton() {}
 
	public static Singleton getInstance() {
		if (uniqueInstance == null) {
			synchronized (Singleton.class) {
				if (uniqueInstance == null) {
					uniqueInstance = new Singleton();
				}
			}
		}
		return uniqueInstance;
	}
}
