package strategy;

public class MiniDuckSimulator {
 
	public static void main(String[] args) {
		
		MallardDuck	mallard = new MallardDuck();
		RubberDuck rubberDuckie = new RubberDuck();
		DecoyDuck decoy = new DecoyDuck();
 
		Duck model = new ModelDuck();
		
		System.out.println("---------綠頭鴨-------------");
		mallard.performQuack();
		mallard.performFly();
		System.out.println("---------橡皮鴨-------------");
		rubberDuckie.performQuack();
		rubberDuckie.performFly();
		System.out.println("---------誘餌鴨-------------");
		decoy.performQuack();
		decoy.performFly();
		System.out.println("---------模型鴨-------------");
		model.performQuack();
		model.performFly();
		//替換飛行行為
		model.setFlyBehavior(new FlyRocketPowered());
		model.performFly();
	}
}
